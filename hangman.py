import random

words = ['orange', 'apple', 'mango', 'banana', 'grapes', 'avocado']
word = random.choice(words)
turn = 5
length = len(word)
print("world length = " + str(length))
guesses = ''
flag = 0
count = 0
guess = input("enter a character: ")
while turn > 0:
    if guess in word[count]:
        print("correct word")
        guesses += guess
        print("\n" + guesses)
        count += 1
        flag = 1
    else:
        print("_")
        print("\t")
        flag = 0
    if len(guesses) == length:
        print("You won")
        break
    if flag == 1:
        guess = input("Enter another character : ")
    else:
        turn -= 1
        print("Wrong input")
        guess = input("Try again : ")

if turn == 0:
    print("You loose, Try next time ")
